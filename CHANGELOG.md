# Overview

This log describes the functionality of tagged versions within the repository.

# Tags

## [Version 1.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.0.0)

### Description
- Initial tagged release. For some reason this project did not have a CHANGELOG file even though
  the system has been in production for years. This releases fixes a bug with the GMA1 label
  as reported by Peter Frey.

### Git Commit Information (the most important ones)
- CHORE: Add CHANGELOG.md file.
- CHORE: Ensure that valves and pumps indicate offline state.
- CHORE: Tweak tube alignment.
- CHORE: Update README.md
- BUG FIX: Create release 1.0.0

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Release Date
* 2024-07-03

