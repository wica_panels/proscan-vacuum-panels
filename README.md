# Overview

This repository provide support for visually monitoring the evolving state of PROSCAN's vacuum systems in real time
from locations outside of PSI.

The solution is based on a technology called Wica which has been developed by PSI's Controls Section. Wica is an open
source project available on [GitHub](https://github.com/paulscherrerinstitute/wica-http).

PSI's Wica installation provides on-demand http streaming access to the EPICS channels in PSI's machine control system.
This project leverages off these information streams to update the state of various widgets that have been incorporated
into SVG Drawings of the vacuum schematics.

# Getting Started

The following procedure is applicable for Desktop PC's running either Mac OSX, Linux or Windows.

First you will need to install the following resources on your system.

1. Node Installation. Available [here](https://nodejs.org/en/download/).
1. IntelliJ IDE. Available [here](https://www.jetbrains.com/idea/download/).

Node is open source and available free of charge.

# Creating a new panel

The SVG support in the IntelliJ IDE is good enough that one can edit the SVG files directly in
the IDE.

# Publishing panels

1. Use the npm 'dev', 'prod', or 'ext' script targets to automatically deploy your changes to the relevant wica servers

