import resolve from '@rollup/plugin-node-resolve';
// import {terser} from 'rollup-plugin-terser';
import copy from "rollup-plugin-copy";
import sourcemaps from 'rollup-plugin-sourcemaps';
import fg from 'fast-glob';

// noinspection JSUnusedGlobalSymbols
export default {
    input: [ 'src/vacuum_support.js' ],
    output: {
        dir: 'build',
        format: 'esm',
        sourcemap: true,
    },
    plugins: [
        copy({
            targets: [
                { src: "src/comet.html", dest: "build" },
                { src: "src/comet.svg", dest: "build" },
                { src: "src/proscan.html", dest: "build" },
                { src: "src/proscan.svg", dest: "build" },
                { src: "src/vacuum_styles.css", dest: "build" },
            ],
        }),
        sourcemaps(),
        resolve(),
        // terser( {} ),
        {
            name: 'watch-external',
            async buildStart() {
                const files = await fg('src/*');
                for(let file of files){
                    this.addWatchFile(file);
                }
            }
        }
    ]
};

