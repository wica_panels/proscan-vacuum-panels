console.debug( "Executing script in vacuum_funcs.js module...");

const OFFLINE_COLOR = "white";
const WARNING_COLOR = "lightcoral";
const OK_COLOR = "mediumseagreen";

let svgDoc = null;

export function init_svg_document_ref()
{
    if ( svgDoc === null ) {
        const svgElement = document.getElementById( "svgHostElement" );
        svgDoc = svgElement.contentDocument;
    }
}

export function update_svg_numeric_value( event, svgEleId, fractionDigits, optUnits = false, optExp = false )
{
    init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if ( ! verify_connection_and_channel( event, () => targetElement.textContent = "ERR" ) )
    {
        return;
    }

    const value = optExp ? event.channelValueLatest[ "val" ].toExponential( fractionDigits ):  event.channelValueLatest.val.toFixed( fractionDigits );
    const units = optUnits ? event.channelMetadata[ "egu" ] : "";
    targetElement.textContent = value + " " + units;
}

export function update_svg_component_color( event, svgElementId, okValue, nokValue )
{
    function handleErrors()
    {
        svgElement.style.setProperty("--pump_fg_color", OFFLINE_COLOR );
        svgElement.style.setProperty( "--valve_fg_color", OFFLINE_COLOR );
    }

    init_svg_document_ref();
    const svgElement = svgDoc.getElementById( svgElementId );
    if ( svgElement == null ) {
        return;
    }

    if ( ! verify_connection_and_channel( event, () => handleErrors() ) )
    {
        return;
    }

    switch ( event.channelValueLatest.val )
    {
        case okValue:
            svgElement.style.setProperty( "--pump_fg_color", OK_COLOR );
            svgElement.style.setProperty( "--valve_fg_color", OK_COLOR );
            break;

        case nokValue:
            svgElement.style.setProperty( "--pump_fg_color", WARNING_COLOR );
            svgElement.style.setProperty( "--valve_fg_color", WARNING_COLOR );
            break;

        default:
            svgElement.style.setProperty( "--pump_fg_color", OFFLINE_COLOR );
            svgElement.style.setProperty( "--valve_fg_color", OFFLINE_COLOR );
            break;
    }
}

export function update_svg_string_value( event, svgEleId, optUnits = false )
{
    init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

   if ( ! verify_connection_and_channel( event, () => targetElement.textContent = "ERR" ) )
   {
       return;
   }

    const units = optUnits ? event.channelMetadata[ "egu" ] : "";
    targetElement.textContent = event.channelValueLatest[ "val" ] + " " + units ;
}

export function update_svg_gauge( event, svgElementId, optSelectSensitiveScale = true )
{
    init_svg_document_ref();
    const svgElement = svgDoc.getElementById( svgElementId );
    if ( svgElement == null ) {
        return;
    }

    if ( ! verify_connection_and_channel( event, () => {
        svgElement.style.setProperty( "--gauge_fg_color", OFFLINE_COLOR );
        svgElement.style.setProperty( "--gauge_bg_color", OFFLINE_COLOR );
    } ) )
    {
        return;
    }

    const DEFAULT_BG_COLOR = "lightgray";
    svgElement.style.setProperty( "--gauge_bg_color", DEFAULT_BG_COLOR );

    const value =  event.channelValueLatest[ "val" ];
    // Range Scale 1 Calculation is derived as follows:
    // - Pressure = 10-3:  gauge_level_in_percent = 95%
    // - Pressure = 10-7:  gauge_level_in_percent = 5%

    // Range Scale 2 Calculation is derived as follows:
    // - Pressure = 10-0:  gauge_level_in_percent = 95%
    // - Pressure = 10-5:  gauge_level_in_percent = 5%

    // noinspection PointlessArithmeticExpressionJS
    const gauge_level_in_percent = optSelectSensitiveScale ?
        clamp(95 + 22.5 * (3 + Math.log10(value)), 0, 100) :
        clamp(95 + 22.5 * (0 + Math.log10(value)), 0, 100);

    // Colourise the gauge depending on whether inside or outside limits
    if ( gauge_level_in_percent >= 100 )
    {
        svgElement.style.setProperty( "--gauge_fg_color", WARNING_COLOR );
    }
    else
    {
        svgElement.style.setProperty( "--gauge_fg_color", OK_COLOR );
    }

    // Set the level of the gauge
    svgElement.style.setProperty( "--gauge_level_in_percent", gauge_level_in_percent );
}

function clamp( num, min, max ) {
    return num <= min ? min : num >= max ? max : num;
}


function verify_connection_and_channel( event, callbackHandler )
{
    const wicaStreamState = event.target.dataset.wicaStreamState;
    const wicaChannelConnectionState = event.target.dataset.wicaChannelConnectionState;

    // The wicaStreamState attribute looks typically like this: 'opened-<n>', where <n> is the connection attempt.
    if ( !wicaStreamState.includes( "opened" ) ) {
        callbackHandler();
        return false;
    }

    // The wicaChannelConnectionState attribute looks like this when the underlying channel is connected: 'connected'.
    if  ( wicaChannelConnectionState !== "connected" ) {
        callbackHandler();
        return false;
    }
    return true;
}
