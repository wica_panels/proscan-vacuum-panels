console.debug("Executing script in proscan_vacuum_subs.js module...");

export function load_subs(template) {
    return template
        .replaceAll('__VACUUM_CYCLOTRON__', VACUUM_CYCLOTRON)
        .replaceAll('__VACUUM_WINDOW_VERT_1__', VACUUM_WINDOW_VERT_1)
        .replaceAll('__VACUUM_WINDOW_VERT_2__', VACUUM_WINDOW_VERT_2)
        .replaceAll('__VACUUM_WINDOW_HORIZ_1__', VACUUM_WINDOW_HORIZ_1)
        .replaceAll('__VACUUM_VALVE_VERT__', VACUUM_VALVE_VERT)
        .replaceAll('__VACUUM_VALVE_HORIZ__', VACUUM_VALVE_HORIZ)
        .replaceAll('__VACUUM_PUMP__', VACUUM_PUMP)
        .replaceAll('__VACUUM_PUMP_TURBO__', VACUUM_PUMP_TURBO)
        .replaceAll('__VACUUM_PUMP_CRYO__', VACUUM_PUMP_CRYO)
        .replaceAll('__VACUUM_GAUGE__', VACUUM_GAUGE)
        .replaceAll('__VACUUM_BUFFER__', VACUUM_BUFFER)
        .replaceAll('__VACUUM_TUBE_VERT__', VACUUM_TUBE_VERT)
        .replaceAll('__VACUUM_TUBE_VERT_50__', VACUUM_TUBE_VERT_50)
        .replaceAll('__VACUUM_TUBE_VERT_100__', VACUUM_TUBE_VERT_100)
        .replaceAll('__VACUUM_TUBE_VERT_150__', VACUUM_TUBE_VERT_150)
        .replaceAll('__VACUUM_TUBE_VERT_200__', VACUUM_TUBE_VERT_200)
        .replaceAll('__VACUUM_TUBE_VERT_250__', VACUUM_TUBE_VERT_250)
        .replaceAll('__VACUUM_TUBE_VERT_400__', VACUUM_TUBE_VERT_400)
        .replaceAll('__VACUUM_TUBE_VERT_450__', VACUUM_TUBE_VERT_450)
        .replaceAll('__VACUUM_TUBE_VERT_500__', VACUUM_TUBE_VERT_500)
        .replaceAll('__VACUUM_TUBE_VERT_700__', VACUUM_TUBE_VERT_700)
        .replaceAll('__VACUUM_TUBE_VERT_850__', VACUUM_TUBE_VERT_850)
        .replaceAll('__VACUUM_TUBE_VERT_1000__', VACUUM_TUBE_VERT_1000)
        .replaceAll('__VACUUM_TUBE_VERT_UPPER__', VACUUM_TUBE_VERT_UPPER)
        .replaceAll('__VACUUM_TUBE_VERT_LOWER__', VACUUM_TUBE_VERT_LOWER)
        .replaceAll('__VACUUM_TUBE_HORIZ__', VACUUM_TUBE_HORIZ)

        .replaceAll('__VACUUM_TUBE_HORIZ_50__', VACUUM_TUBE_HORIZ_50)
        .replaceAll('__VACUUM_TUBE_HORIZ_100__', VACUUM_TUBE_HORIZ_100)
        .replaceAll('__VACUUM_TUBE_HORIZ_200__', VACUUM_TUBE_HORIZ_200)
        .replaceAll('__VACUUM_TUBE_HORIZ_250__', VACUUM_TUBE_HORIZ_250)
        .replaceAll('__VACUUM_TUBE_HORIZ_350__', VACUUM_TUBE_HORIZ_350)
        .replaceAll('__VACUUM_TUBE_HORIZ_400__', VACUUM_TUBE_HORIZ_400)
        .replaceAll('__VACUUM_TUBE_HORIZ_600__', VACUUM_TUBE_HORIZ_600)
        .replaceAll('__VACUUM_TUBE_HORIZ_800__', VACUUM_TUBE_HORIZ_800)
        .replaceAll('__VACUUM_TUBE_HORIZ_900__', VACUUM_TUBE_HORIZ_900)
        .replaceAll('__VACUUM_TUBE_HORIZ_1000__', VACUUM_TUBE_HORIZ_1000)

        .replaceAll('__VACUUM_TUBE_HORIZ_LEFT__', VACUUM_TUBE_HORIZ_LEFT)
        .replaceAll('__VACUUM_TUBE_HORIZ_RIGHT__', VACUUM_TUBE_HORIZ_RIGHT)
        .replaceAll('__VACUUM_TUBE_T_UPPER__', VACUUM_TUBE_T_UPPER)
        .replaceAll('__VACUUM_TUBE_T_LOWER__', VACUUM_TUBE_T_LOWER)
        .replaceAll('__VACUUM_TUBE_X__', VACUUM_TUBE_X)
        .replaceAll('__VACUUM_TUBE_HEMI_LEFT__', VACUUM_TUBE_HEMI_LEFT)
        .replaceAll('__VACUUM_TUBE_HEMI_RIGHT__', VACUUM_TUBE_HEMI_RIGHT)
        .replaceAll('__VACUUM_TUBE_HEMI_UP__', VACUUM_TUBE_HEMI_UP)
        .replaceAll('__VACUUM_TUBE_HEMI_DOWN__', VACUUM_TUBE_HEMI_DOWN)
        .replaceAll('__VACUUM_MAGNET__', VACUUM_MAGNET)
        .replaceAll('__VACUUM_TARGET__', VACUUM_TARGET)
        .replaceAll('__VACUUM_MEASUREMENT_POINT__', VACUUM_MEASUREMENT_POINT)
        .replaceAll('__SVG_STYLES__', SVG_STYLES);
}

const VACUUM_CYCLOTRON =
    `
    <!-- Vacuum Cyclotron. Supported vars: 'cyclotron_fg_color', 'cyclotron_bg_color', 'cyclotron_border_color. -->
    <symbol id="vacuum_cyclotron" viewBox="0 0 100 100">
        <title>vacuum magnet</title>
        <rect x="12" y="2" rx="12" width="76" height="96" fill="var(--cyclotron_bg_color,lightgray)" stroke="var(--cyclotron_border_color,black)" stroke-width="4"/>
        <circle cx="50" cy="50" r="25" fill="var(--cyclotron_fg_color,lightgray)"/>
    </symbol>
    `;

const VACUUM_WINDOW_VERT_1 =
    `
    <!-- Vacuum Window. Supported vars: 'window_fg_color', 'window_bg_color', 'window_border_color. -->
    <symbol id="vacuum_window_vert_1" viewBox="0 0 100 100">
        <title>vacuum window: vertical, type 1</title>
        <rect x="22" y="2" rx="12" width="56" height="96" fill="var(--window_bg_color,lightgray)" stroke="var(--window_border_color,black)" stroke-width="4"/>
        <line x1="45" y1="80" x2="45" y2="20" stroke="var(--window_fg_color,lightgray)" stroke-width="4"/>
        <line x1="55" y1="80" x2="55" y2="20" stroke="var(--window_fg_color,lightgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_WINDOW_VERT_2 =
    `
    <!-- Vacuum Window. Supported vars: 'window_fg_color', 'window_bg_color', 'window_border_color. -->
    <symbol id="vacuum_window_vert_2" viewBox="0 0 100 100">
        <title>vacuum window: vertical, type 2</title>
        <rect x="22" y="2" rx="12" width="56" height="96" fill="var(--window_bg_color,lightgray)" stroke="var(--window_border_color,black)" stroke-width="4"/>
        <line x1="50" y1="80" x2="50" y2="20" stroke="var(--window_fg_color,lightgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_WINDOW_HORIZ_1 =
    `
    <!-- Vacuum Window. Supported vars: 'window_fg_color', 'window_bg_color', 'window_border_color. -->
    <symbol id="vacuum_window_horiz_1" viewBox="0 0 100 100">
        <title>vacuum window: horizontal, type 1</title>
        <rect x="2" y="22" rx="12" width="96" height="56" fill="var(--valve_bg_color,lightgray)"
              stroke="var(--valve_border_color,black)" stroke-width="4"/>
        <line x1="20" y1="50" x2="80" y2="50" stroke="var(--window_fg_color,lightgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_VALVE_VERT =
    `
    <!-- Vacuum Valve: Vertical. Supported vars: 'component_fill_color'. -->
    <symbol id="vacuum_valve_vert" viewBox="0 0 100 100">
      <title>vacuum valve: vertical</title>
      <rect x="22" y="2" rx="12" width="56" height="96" fill="var(--valve_bg_color,lightgray)" stroke="var(--valve_border_color,black)" stroke-width="4"/>
      <path d="M 30 15 H 70 L 30 85 H 70 L 30 15" fill="var(--valve_fg_color,darkgray)"/>
    </symbol>
    `;

const VACUUM_VALVE_HORIZ =
    `
    <!-- Vacuum Valve: Horizontal. Supported vars: 'valve_fg_color', 'valve_bg_color', 'valve_border_color. -->
    <symbol id="vacuum_valve_horiz" viewBox="0 0 100 100">
      <title>vacuum valve: horizontal</title>
      <rect x="2" y="22" rx="12" width="96" height="56" fill="var(--valve_bg_color,lightgray)" stroke="var(--valve_border_color,black)" stroke-width="4"/>
      <path d="M 15 30 V 70 L 85 30 V 70 L 15 30" fill="var(--valve_fg_color,darkgray)"/>
    </symbol>
`;

const VACUUM_PUMP =
    `
    <!-- Vacuum Pump. Supported vars: 'pump_fg_color', 'pump_bg_color', 'pump_border_color. -->
    <symbol id="vacuum_pump" viewBox="0 0 100 100">
        <title>vacuum pump</title>
        <rect x="2" y="2" rx="12" width="96" height="96" fill="var(--pump_bg_color,lightgray)" stroke="var(--pump_border_color,black)" stroke-width="4"/>
        <circle cx="50" cy="50" r="35" fill="var(--pump_fg_color,darkgray)"/>
        <line x1="45" y1="90" x2="30" y2="10" stroke="lightgray" stroke-width="4"/>
        <line x1="55" y1="90" x2="70" y2="10" stroke="lightgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_PUMP_TURBO =
    `
    <!-- Vacuum Turbo Pump. Supported vars: 'pump_fg_color', 'pump_bg_color', 'pump_border_color. -->
    <symbol id="vacuum_pump_turbo" viewBox="0 0 100 100">
      <title>vacuum pump: turbo</title>
      <rect x="2" y="2" rx="12" width="96" height="96" fill="var(--pump_bg_color,lightgray)" stroke="var(--pump_border_color,black)" stroke-width="4"/>
      <circle cx="50" cy="50" r="35" fill="var(--pump_fg_color,darkgray)"/>
      <circle cx="50" cy="50" r="5" fill="var(--pump_bg_color,lightgray)"/>
      <line x1="45" y1="90" x2="30" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
      <line x1="55" y1="90" x2="70" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_PUMP_CRYO =
    `
    <!-- Vacuum Cryo Pump. Supported vars: 'component_fill_color'. -->
    <symbol id="vacuum_pump_cryo" viewBox="0 0 100 100">
        <title>vacuum pump: cryo</title>
        <rect x="2" y="2" rx="12" width="96" height="96" fill="var(--pump_bg_color,lightgray)" stroke="var(--pump_border_color,black)" stroke-width="4"/>
        <circle cx="50" cy="50" r="35" fill="var(--pump_fg_color,darkgray)"/>
        <line x1="45" y1="90" x2="30" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
        <line x1="55" y1="90" x2="70" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
        <circle cx="50" cy="50" r="10" fill="var(--pump_bg_color,lightgray)"/>
        <text x="50" y="54" font-size="13" text-anchor="middle">K</text>
    </symbol>
`;

// noinspection CssUnresolvedCustomProperty
const VACUUM_GAUGE =
    `
    <!-- Vacuum Gauge. Supported vars: 'gauge_level_in_percent', 'gauge_scale_1', 'gauge_scale_2', -->
    <!-- 'gauge_bg_color', 'gauge_fg_color', 'gauge_border_color' -->
    <symbol id="vacuum_gauge" viewBox="0 0 100 250">
      <title>vacuum gauge</title>
      <rect x="2" y="2" rx="12" width="96" height="196" fill="var(--gauge_bg_color,lightgray)" stroke="var(--gauge_border_color,black)" stroke-width="4"/>
      <rect x="20" y="30" width="40" height="140" fill="var(--gauge_bg_color,lightgray)" stroke="var(--gauge_border_color,black)" stroke-width="3"/>
      <rect x="22" width="36" fill="var(--gauge_fg_color,mediumseagreen)" style="height: calc( 2px + var(--gauge_level_in_percent) * 1.36px); y:calc( 167px - var(--gauge_level_in_percent) * 1.36px"/>
      <!-- Range Scale Ticks -->
      <line x1="60" y1="40" x2="65" y2="40" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="70" x2="65" y2="70" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="100" x2="65" y2="100" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="130" x2="65" y2="130" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="160" x2="65" y2="160" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <!-- Range 1 Scale Labels -->
      <text x="65" y="44" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-3</text>
      <text x="65" y="74" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-4</text>
      <text x="65" y="104" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-5</text>
      <text x="65" y="134" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-6</text>
      <text x="65" y="164" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-7</text>
      <!-- Range 2 Scale Labels -->
      <text x="65" y="44" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-0</text>
      <text x="65" y="74" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-1</text>
      <text x="65" y="104" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-2</text>
      <text x="65" y="134" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-3</text>
      <text x="65" y="164" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-4</text>
    </symbol>
`;

const VACUUM_BUFFER =
    `
    <!-- Vacuum Buffer -->
    <symbol id="vacuum_buffer" viewBox="0 0 100 100">
      <title>vacuum buffer</title>
      <rect x="0" y="0" width="100" height="100" fill-opacity="0"/>
      <ellipse cx="50" cy="50" rx="40" ry="10" fill-opacity="0" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
      <line x1="50" y1="0" x2="50" y2="40" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
      <line x1="50" y1="60" x2="50" y2="100" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_VERT =
    `
    <!-- Vacuum Tube: Vertical -->
    <symbol id="vacuum_tube_vert" viewBox="0 0 100 100">
        <title>vacuum tube: vertical tube</title>
        <line x1="50" y1="0" x2="50" y2="100" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_VERT_UPPER =
    `
    <!-- Vacuum Tube: Vertical Upper -->
    <symbol id="vacuum_tube_vert_upper" viewBox="0 0 100 100">
        <title>vacuum tube: vertical upper</title>
        <line x1="50" y1="0" x2="50" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_VERT_LOWER =
    `
    <!-- Vacuum Tube: Vertical Lower -->
    <symbol id="vacuum_tube_vert_lower" viewBox="0 0 100 100">
        <title>vacuum tube: vertical lower</title>
        <line x1="50" y1="50" x2="50" y2="100" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HORIZ =
    `
    <!-- Vacuum Tube: Horizontal -->
    <symbol id="vacuum_tube_horiz" viewBox="0 0 100 100">
        <title>vacuum tube: horizontal</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="var(--tube_stroke_color,darkgray)"  stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HORIZ_LEFT =
    `
    <!-- Vacuum Tube: Horizontal Left -->
    <symbol id="vacuum_tube_horiz_left" viewBox="0 0 100 100">
        <title>vacuum tube: horizontal left</title>
        <line x1="0" y1="50" x2="50" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HORIZ_RIGHT =
    `
    <!-- Vacuum Tube: Horizontal Right -->
    <symbol id="vacuum_tube_horiz_right" viewBox="0 0 100 100">
        <title>vacuum tube: horizontal right</title>
        <line x1="50" y1="50" x2="100" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_MAGNET =
    `
    <!-- Vacuum Magnet -->
    <symbol id="vacuum_magnet" viewBox="0 0 100 100">
      <title>vacuum magnet</title>
      <rect x="12" y="2" rx="12" width="76" height="96" fill="var(--magnet_bg_color,lightgray)" stroke="var(--magnet_border_color,black)" stroke-width="4"/>
      <rect x="25" y="25" rx="12" width="50" height="50" fill="var(--magnet_fg_color,darkgray)"/>
    </symbol>
    `;

const VACUUM_TARGET =
    `
    <!-- Vacuum Target -->
    <symbol id="vacuum_target" viewBox="0 0 100 100">
      <title>vacuum target</title>
      <rect x="22" y="2" rx="12" width="56" height="96" fill="var(--valve_bg_color,lightgray)" stroke="var(--valve_border_color,black)" stroke-width="4"/>
      <ellipse cx="50" cy="50" rx="10" ry="40" fill-opacity="0" stroke="darkgray" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_MEASUREMENT_POINT =
    `
    <!-- Vacuum Measurement Point. Supported vars: 'meas_fg_color', 'meas_bg_color', 'meas_border_color. -->
    <symbol id="vacuum_measurement_point" viewBox="0 0 50 50">
        <title>vacuum measurement point</title>
        <rect x="2" y="2" rx="6" width="46" height="46" fill="var(--meas_bg_color,lightgray)" stroke="var(--meas_border_color,black)" stroke-width="4"/>
        <circle cx="25" cy="25" r="17" fill="var(--meas_fg_color,darkgray)"/>
        <line x1="22" y1="45" x2="15" y2="5" stroke="lightgray" stroke-width="4"/>
        <line x1="28" y1="45" x2="35" y2="5" stroke="lightgray" stroke-width="4"/>
        <line x1="7" y1="25" x2="43" y2="25" stroke="lightgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_VERT_50 =
    `
    <!-- Vacuum Tube: Vertical 50. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_50" viewBox="0 0 100 50">
        <title>vacuum tube: vertical tube, length 50</title>
        <line x1="50" y1="0" x2="50" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_100 =
    `
    <!-- Vacuum Tube: Vertical 100. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_100" viewBox="0 0 100 100">
        <title>vacuum tube: vertical tube, length 100</title>
        <line x1="50" y1="0" x2="50" y2="100" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_150 =
    `
    <!-- Vacuum Tube: Vertical 150. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_150" viewBox="0 0 100 150">
        <title>vacuum tube: vertical tube, length 150</title>
        <line x1="50" y1="0" x2="50" y2="150" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_200 =
    `
    <!-- Vacuum Tube: Vertical 200. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_200" viewBox="0 0 100 200">
        <title>vacuum tube: vertical tube, length 200</title>
        <line x1="50" y1="0" x2="50" y2="200" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_250 =
    `
    <!-- Vacuum Tube: Vertical 200. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_250" viewBox="0 0 100 250">
        <title>vacuum tube: vertical tube, length 250</title>
        <line x1="50" y1="0" x2="50" y2="250" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_400 =
    `
    <!-- Vacuum Tube: Vertical 400. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_400" viewBox="0 0 100 400">
        <title>vacuum tube: vertical tube, length 400</title>
        <line x1="50" y1="0" x2="50" y2="400" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_450 =
    `
    <!-- Vacuum Tube: Vertical 450. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_450" viewBox="0 0 100 450">
        <title>vacuum tube: vertical tube, length 450</title>
        <line x1="50" y1="0" x2="50" y2="450" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_500 =
    `
    <!-- Vacuum Tube: Vertical 500. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_500" viewBox="0 0 100 500">
        <title>vacuum tube: vertical tube, length 500</title>
        <line x1="50" y1="0" x2="50" y2="500" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_700 =
    `
    <!-- Vacuum Tube: Vertical 700. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_700" viewBox="0 0 100 700">
        <title>vacuum tube: vertical tube, length 700</title>
        <line x1="50" y1="0" x2="50" y2="700" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_850 =
    `
    <!-- Vacuum Tube: Vertical 850. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_850" viewBox="0 0 100 850">
        <title>vacuum tube: vertical tube, length 850</title>
        <line x1="50" y1="0" x2="50" y2="850" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_VERT_1000 =
    `
    <!-- Vacuum Tube: Vertical 1000. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_vert_1000" viewBox="0 0 100 1000">
        <title>vacuum tube: vertical tube, length 1000</title>
        <line x1="50" y1="0" x2="50" y2="1000" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_50 =
    `
    <!-- Vacuum Tube: Horizontal 50. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_horiz_50" viewBox="0 0 50 100">
        <title>vacuum tube: horizontal tube, length 50</title>
        <line x1="0" y1="50" x2="50" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_100 =
    `
    <!-- Vacuum Tube: Horizontal 100. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_horiz_100" viewBox="0 0 100 100">
        <title>vacuum tube: horizontal tube, length 100</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_200 =
    `
    <!-- Vacuum Tube: Horizontal 200 -->
    <symbol id="vacuum_tube_horiz_200" viewBox="0 0 200 100">
        <title>vacuum tube: horizontal tube, length 200</title>
        <line x1="0" y1="50" x2="200" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_250 =
    `
    <!-- Vacuum Tube: Horizontal 250 -->
    <symbol id="vacuum_tube_horiz_250" viewBox="0 0 250 100">
        <title>vacuum tube: horizontal tube, length 250</title>
        <line x1="0" y1="50" x2="250" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_350 =
    `
    <!-- Vacuum Tube: Horizontal 350 -->
    <symbol id="vacuum_tube_horiz_350" viewBox="0 0 350 100">
        <title>vacuum tube: horizontal tube, length 350</title>
        <line x1="0" y1="50" x2="350" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_400 =
    `
    <!-- Vacuum Tube: Horizontal 400. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_horiz_400" viewBox="0 0 400 100">
        <title>vacuum tube: horizontal tube, length 400</title>
        <line x1="0" y1="50" x2="400" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_600 =
    `
    <!-- Vacuum Tube: Horizontal 600. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_horiz_600" viewBox="0 0 600 100">
        <title>vacuum tube: horizontal tube, length 600</title>
        <line x1="0" y1="50" x2="600" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_800 =
    `
    <!-- Vacuum Tube: Horizontal 800. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_horiz_800" viewBox="0 0 800 100">
        <title>vacuum tube: horizontal tube, length 800</title>
        <line x1="0" y1="50" x2="800" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_900 =
    `
    <!-- Vacuum Tube: Horizontal 900. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_horiz_900" viewBox="0 0 900 100">
        <title>vacuum tube: horizontal tube, length 900</title>
        <line x1="0" y1="50" x2="900" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HORIZ_1000 =
    `
    <!-- Vacuum Tube: Horizontal 1000. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_horiz_1000" viewBox="0 0 1000 100">
        <title>vacuum tube: horizontal tube, length 1000</title>
        <line x1="0" y1="50" x2="1000" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_T_UPPER =
    `
    <!-- Vacuum Tube: T-Section Right Way Up-->
    <symbol id="vacuum_tube_tee_normal" viewBox="0 0 100 100">
        <title>vacuum tube: T-section normal</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
        <line x1="50" y1="0" x2="50" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_T_LOWER =
    `    
    <!-- Vacuum Tube: T-Section Inverted. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_tee_invert" viewBox="0 0 100 100">
        <title>vacuum tube: T-section inverted</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
        <line x1="50" y1="50" x2="50" y2="100" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_X =
    `
    <!-- Vacuum Tube: X-Section. Supported vars: 'tube_stroke_color' -->
    <symbol id="vacuum_tube_x" viewBox="0 0 100 100">
        <title>vacuum tube: X-section</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
        <line x1="50" y1="0" x2="50" y2="100" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HEMI_LEFT =
    `
<!-- Vacuum Tube: Hemisphere Left. Supported vars: 'tube_stroke_color' -->
<!--suppress XmlDefaultAttributeValue -->
<symbol id="vacuum_tube_hemi_left" viewBox="0 0 100 100" overflow="visible">
    <title>vacuum tube: hemisphere facing left</title>
    <path d="M 100 0 A 50 50 0 0 0 100 100" fill-opacity="0" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
</symbol>
    `;

const VACUUM_TUBE_HEMI_RIGHT =
    `
    <!-- Vacuum Tube: Hemisphere Right. Supported vars: 'tube_stroke_color' -->
    <!--suppress XmlDefaultAttributeValue -->
    <symbol id="vacuum_tube_hemi_right" viewBox="0 0 100 100" overflow="visible">
        <title>vacuum tube: hemisphere facing right</title>
        <path d="M 0 0 A 50 50 0 0 1 0 100" fill-opacity="0" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_TUBE_HEMI_UP =
    `
    <!-- Vacuum Tube: Hemisphere Up. Supported vars: 'tube_stroke_color' --> -->
    <!--suppress XmlDefaultAttributeValue -->
    <symbol id="vacuum_tube_hemi_up" viewBox="0 0 100 100" overflow="visible">
        <title>vacuum tube: hemisphere facing up</title>
    <path d="M 100 0 A 50 50 0 0 0 100 100" fill-opacity="0" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4" transform="rotate(90,50,50)"/>
    </symbol>
    `;

const VACUUM_TUBE_HEMI_DOWN =
    `
    <!-- Vacuum Tube: Hemisphere Down. Supported vars: 'tube_stroke_color' -->
    <!--suppress XmlDefaultAttributeValue -->
    <symbol id="vacuum_tube_hemi_down" viewBox="0 0 100 100" overflow="visible">
        <title>vacuum tube: hemisphere facing down</title>
        <path d="M 0 0 A 50 50 0 0 1 0 100" fill-opacity="0" stroke="var(--tube_stroke_color,darkgray)" stroke-width="4" transform="rotate(90,50,50)" />
    </symbol>
    `;

const SVG_STYLES =
    `
    :root {
       --pump_fg_color: black;
       --pump_bg_color: lightgray;
       --pump_border_color: black;
       --valve_fg_color: black;
       --valve_bg_color: lightgray;
       --valve_border_color: black;
       --gauge_fg_color: black;
       --gauge_bg_color: lightgray;
       --gauge_border_color: black;
       --gauge_font_size: 14px;
       --tube_stroke_color: darkslategrey;
       --magnet_fg_color: darkblue;
       --magnet_bg_color: lightgray;
       --magnet_border_color: black;
       --window_fg_color: black;
       --window_bg_color: lightgray;
       --window_border_color: black;
       --cyclotron_fg_color: darkblue;
       --cyclotron_bg_color: lightgray;
       --cyclotron_border_color: black;
    }

    .hv {
      --tube_stroke_color: lemonchiffon;
    }
   
    .nav_box {
        fill: lightgray;
        stroke: darkgray;
        stroke-width:4;
        rx: 8;
    }
    
    .nav_box:hover {
       fill: royalblue;
    }
    
    .nav_link {
       cursor: pointer;
    }
    
    .nav_label {
        font: bold 40px sans-serif;
        fill: darkblue;
        pointer-events: none;
    }
    
    .cavity {
       fill: darkslategrey;
       stroke: darkslategrey;
       stroke-width:4;
        rx: 12;
    }
    
    .cavity_label {
        font: bold 40px sans-serif;
        fill: lightgray;
    }

    .info_box {
        fill: lightgray;
        stroke: darkgray;
        stroke-width:4;
    }
    
    .tube {
        stroke: darkgray;
        stroke-width: 5px;
    }
    
    .banner {
        font: bold 42px sans-serif;
        fill: darkblue;
    }
    
    .label_l {
        font: bold 32px sans-serif;
        fill: darkblue;
    }
    
    .label_m {
        font: bold 24px sans-serif;
        fill: darkblue;
    }
    
   .time_value {
        font: 30px sans-serif;
        fill: blue;
    }

    .pressure_value {
        font: 24px sans-serif;
        fill: blue;
    }

    .value {
        font: 24px sans-serif;
        fill: blue;
    }
    
    a:link {
        cursor: pointer;
    }   
`;

