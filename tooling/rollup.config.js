import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";
import {load_subs} from "../src/subs/proscan_vacuum_subs.js";

// noinspection JSUnusedGlobalSymbols
export default () => {

    const buildTarget = process.env.BUILD_DEV ? "build/dev" :
                               process.env.BUILD_PROD ? "build/prod" :
                               process.env.BUILD_EXT ? "build/ext" : "";

    return [
        {
            input: 'src/js/vacuum-support.js',
            output: {
                dir:  buildTarget,
                format: 'es',
                sourcemap: true,
            },
            plugins: [
                resolve(),
                commonjs(),
                copy( {
                    targets: [
                        // Install COMET
                        { src: "src/html/comet.html", dest: buildTarget },
                        {
                            src: "src/templates/comet.template.svg", dest: buildTarget,
                            transform: (contents) => load_subs( contents.toString() ),
                            rename: "comet.svg",
                        },
                        // Install PROSCAN
                        { src: "src/html/proscan.html", dest: buildTarget },
                        {
                            src: "src/templates/proscan.template.svg", dest: buildTarget,
                            transform: (contents) => load_subs( contents.toString() ),
                            rename: "proscan.svg",
                        },

                    ],
                })
            ]
        }
    ]
}
